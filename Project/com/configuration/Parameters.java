/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.configuration;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ilya
 */
public class Parameters {
    private final String patch;
    private StringBuffer xml = new StringBuffer(); 
    private final ArrayList<String> NameParam;
    private final ArrayList<StringBuffer> ValueParam;

    public ArrayList<String> getNameParam() {
        return NameParam;
    }

    public ArrayList<StringBuffer> getValueParam() {
        return ValueParam;
    }
    
    public StringBuffer getValueParam(String name){
//        System.out.println("com.configurationv1.Parameters.getValueParam() name = "+name);
        int  num = NameParam.indexOf(name);
//        System.out.println("com.configurationv1.Parameters.getValueParam() num = "+num);
        return ValueParam.get(num);
    }
    
    public ArrayList<String> getArrayValueParam(String name){        
        boolean Data = true;  
        StringBuffer param = ValueParam.get(NameParam.indexOf(name));
        ArrayList<String> ArrayValueParam = new ArrayList<>();
        while(Data){
        if(param.indexOf("<") != -1){
        String tag = param.substring(param.indexOf("<")+1,param.indexOf(">"));
        String valueTag;
        if(tag.length()> 0){
            int Start = param.indexOf("<"+tag+">");
            int End = param.indexOf("</"+tag+">");
            valueTag = param.substring(Start+2+tag.length(),End);
            param.delete(Start, End+3+tag.length());
        }else valueTag = "Not found value parametrs name:" +tag;
        ArrayValueParam.add(valueTag);
        }else Data = false;         
        }        
        return ArrayValueParam;
    }
    
    public Parameters(String patch){
        this.NameParam = new ArrayList<>();
        this.ValueParam = new ArrayList<>();
        this.patch = patch;
    }
    
    public void Initialize(){
        try {
            System.out.println("Start LoadFileParam");
            LoadFileParam();
            System.out.println("End LoadFileParam");
        } catch (IOException ex) {
            Logger.getLogger(Parameters.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Start ProccesingData");
        ProccesingData();
        System.out.println("End ProccesingData");
    }
    
    private void ProccesingData(){
            Deserialization();                
    }
    
    private void Deserialization(){
        boolean Data = true;                
        while(Data){
        if(xml.indexOf("<") != -1){
        String tag = xml.substring(xml.indexOf("<")+1,xml.indexOf(">"));
        StringBuffer valueTag = new StringBuffer();
        if(tag.length()> 0){
            int Start = xml.indexOf("<"+tag+">");
            int End = xml.indexOf("</"+tag+">");
//            System.out.println("Start tag: <"+tag+"> index: "+Start);
//            System.out.println("Start tag: </"+tag+"> index: "+End);
            
            valueTag.append(xml.substring(Start+2+tag.length(),End));
            xml.delete(Start, End+3+tag.length());
        }else valueTag.append("Not found value parametrs name:").append(tag);
        NameParam.add(tag);
        ValueParam.add(valueTag);
        }else Data = false;         
        }
    }
    
     private void LoadFileParam() throws FileNotFoundException, IOException {
        String CodeStr;
        CodeStr = "UTF-8";
        String buffer = "";               
        BufferedReader reader = null;
        StringBuffer XMLOut =new StringBuffer();
        try{
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(patch), CodeStr));        
        while(buffer != null){
            buffer = reader.readLine();  
            if (buffer != null){        
                XMLOut.append(buffer);
            }
        }         
        int Start = XMLOut.indexOf("<Parametrs>")+11;
        int End = XMLOut.indexOf("</Parametrs>");        
        xml.append(XMLOut.substring(Start,End));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Parameters.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(reader != null)
                reader.close();
        }
    }
}
