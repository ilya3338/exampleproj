/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.configuration;


/**
 *
 * @author Ilya
 */
public class ConfigPatch {
    // получение домашней директории из переменых среды иначе путь по умолчанию.
    private static final String VALUE = System.getenv("OWNER_INTEGRATION") == null ? "C:\\infor\\INTEGRATION\\" : System.getenv("OWNER_INTEGRATION")+"\\";
    
    // Пути К файлам настроек.
    public static final String PATCHEDI = VALUE+"Outbound\\new\\";
    public static final String PATCHARHIVE = VALUE+"Outbound\\arhive\\";
    public static final String PATCHCONST = VALUE+"config\\constant\\constDEV.xml";
    public static final String SKUADD = VALUE+"config\\event\\SKUADD.xml";
    public static final String PATCHJEXCEEDSOCKETINTERFACE = VALUE+"config\\JExceedSocketInterface\\";
    public static final String DIRECTORYINBOUNDNEW = VALUE+"Inbound\\new";
    public static final String DIRECTORYINBOUNDNEWEXCEL = VALUE+"Inbound\\EXCELnew\\";
    public static final String RECEIPTIN = VALUE+"config\\event\\Receipt(C78).xml";
    public static final String RECEIPTOUT = VALUE+"config\\event\\Receipt(C81).xml";
    public static final String RECEIPTOUTMOVEC21 = VALUE+"config\\event\\MoveInReceipt(C21a).xml";
    public static final String ORDER = VALUE+"config\\event\\Order(C76).xml";
    public static final String ORDERC16 = VALUE+"config\\event\\Order(C16).xml";
    public static final String ORDERC77PICKING = VALUE+"config\\event\\Order(C77).xml";
    public static final String ORDERC77PACKING = VALUE+"config\\event\\Order(C77).xml";
    public static final String ORDERC77DISPATCHED = VALUE+"config\\event\\Order(C77(DISPATCHED)).xml";
    public static final String RECEIPTOUTVOZVRAT = VALUE+"config\\event\\ReceiptReturn(C77(DISPATCHED)).xml";
    public static final String HOLDREMOVEDBYID = VALUE+"config\\event\\HoldRemovedById(C21a).xml";
    public static final String HOLDBYID = VALUE+"config\\event\\HoldByID(C21a).xml";
    public static final String MOVE = VALUE+"config\\event\\Move(C21a).xml";   
    public static final String HOLDBYLOT = VALUE+"config\\event\\HoldByLot(C21a).xml";
    public static final String ADJUSTMENT = VALUE+"config\\event\\Adjustment(C21a).xml";
    public static final String LIQUIDATION = VALUE+"config\\event\\liquidation(C21a).xml";
    public static final String HOLDREMOVEDBYLOT = VALUE+"config\\event\\HoldRemovedByLot(C21a).xml";
    public static final String BALANCEC89KZ01 = VALUE+"config\\event\\Balance(C89).xml";
    public static final String BALANCEC89KZ99 = VALUE+"config\\event\\BalanceKZ99(C89).xml";
    public static final String MOVEC21A = VALUE+"config\\event\\Move(C21a).xml";
    public static final String RC92 = VALUE+"config\\event\\Refresh(C92).xml";
    public static final String TRANSFERC92 = VALUE+"config\\event\\For transferDetail.xml";
    public static final String HOLDC92 = VALUE+"config\\event\\ForBatchHold.xml";
    public static final String EMAIL = VALUE+"config\\EmailDat.xml";
    public static final String DIRECTORYINBOUNDSUCESS = VALUE+"Inbound\\success\\";
    public static final String DIRECTORYINBOUNDERROR = VALUE+"Inbound\\Error\\";
    public static final String CONTODB = VALUE+"config\\connection\\ConnectionToDB.xml";
    
    //Получение параметров для подключение и рассылки.
    public static final String CONLOCALHOST = "jdbc:sqlserver://localhost:1433;databaseName=PRD;";
    public static final String CONDEVVIRT1 = "jdbc:sqlserver://DEVVIRT1:1433;databaseName=PRD;";
    public static final String CONURL = SETCONURL();
    public static final String USERWH1 = SETUSERWH1();
    public static final String PASSWH1 = SETPASSWH1();
    
    public static final String DEVMAIL = "";
    public static String OWNERMAILKZ01 = ""; 
    public static String OWNERMAILKZ21 = ""; 
    
    private static String SETCONURL(){
        Parameters paramObj = new Parameters(CONTODB);
        paramObj.Initialize();        
        return new String(paramObj.getValueParam("CONURL"));
    }
    private static String SETUSERWH1(){
        Parameters paramObj = new Parameters(CONTODB);
        paramObj.Initialize();        
        return new String(paramObj.getValueParam("USERWH1"));
    }
    private static String SETPASSWH1(){
        Parameters paramObj = new Parameters(CONTODB);
        paramObj.Initialize();        
        return new String(paramObj.getValueParam("PASSWH1"));
    }
     
    
   
}
