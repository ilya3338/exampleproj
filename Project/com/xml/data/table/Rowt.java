package com.xml.data.table;
import java.util.ArrayList;
/**
 *
 * @author Ilya
 */
public class Rowt {
    
    public final ArrayList<String> columns = new ArrayList<>();
    
    public void addColumns(String cell) { //Добавление ячейки в строку
       columns.add(cell);
    }     
    
    public ArrayList<String> getColumns() { // Получение всей строки
        return columns;
    }    
    
}
