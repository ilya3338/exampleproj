/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xml.data.table;

import java.util.ArrayList;

/**
 *
 * @author ilyab
 */
public class witchQualifier {
    private final String PatchTagDataRow;
    private final String PatchTagData;
    private final ArrayList<String> PatchTagParam;
    private final ArrayList<String> TagParamValue;

    public witchQualifier(String data) {
        this.PatchTagDataRow = getValueTagWithQualifierRow(data);
        this.PatchTagData = getValueTagWithQualifier(data);
        this.PatchTagParam = new ArrayList();
        this.TagParamValue = new ArrayList();
        getValueTagWithQualifierParam(data);
    }
    
    private String getValueTagWithQualifierRow(String tag){
         if(tag.indexOf(".[with qualifier]")>0){  
            return tag.substring(0, tag.indexOf(".[with qualifier]"));
         }else
            return tag;
     }
    
    private String getValueTagWithQualifier(String tag){
         if(tag.indexOf("[with qualifier]")>0){  
            return tag.substring(tag.indexOf(")/")+2);
         }else
            return tag;
     }
       
    private void getValueTagWithQualifierParam(String tag){
        String params = tag.substring(tag.indexOf(".[with qualifier](")+ 18,tag.indexOf(")/"));
        int index = 0;
        do{
             int indexbr = params.indexOf(";", index);
             if(indexbr < 0){
                String data = getDataParam(params,index);
                PatchTagParam.add(getParam(data));
                TagParamValue.add(getParamValue(data));
                break;
             }else
             {
                String data = getDataParam(params,index);
                PatchTagParam.add(getParam(data));
                TagParamValue.add(getParamValue(data));
                index = indexbr+1;
             }
        }while(true);               
     }
    
    private String getDataParam(String data,int index){
        if(data.indexOf(";",index)>0){
            return data.substring(index,data.indexOf(";",index));
        }else
            return data.substring(index);
    }
    
    private String getParam(String data){        
        return data.substring(0,data.indexOf("='"));        
    }
    
    private String getParamValue(String data){        
        return data.substring(data.indexOf("'")+1,data.lastIndexOf("'"));        
    }    

    public String getPatchTagDataRow() {
        return PatchTagDataRow;
    }

    public String getPatchTagData() {
        return PatchTagData;
    }

    public ArrayList<String> getPatchTagParam() {
        return PatchTagParam;
    }

    public ArrayList<String> getTagParamValue() {
        return TagParamValue;
    }  
    
}
