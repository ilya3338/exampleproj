/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xml.data.table;

import java.util.ArrayList;

/**
 *
 * @author Ilya
 */
public class tag {
    private final ArrayList<String> Params;
    private final ArrayList<String> ParamsValue;
    private final String NameTag;
    private final String Param;

    public tag(ArrayList<String> Params, ArrayList<String> ParamsValue, String NameTag, String Param) {
        this.Params = Params;
        this.ParamsValue = ParamsValue;
        this.NameTag = NameTag;
        this.Param = Param;
    }

    public String getParam() {
        return Param;
    }

    public ArrayList<String> getParams() {
        return Params;
    }

    public ArrayList<String> getParamsValue() {
        return ParamsValue;
    }

    public String getNameTag() {
        return NameTag;
    }
    
}
