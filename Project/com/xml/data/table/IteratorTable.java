/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xml.data.table;

import java.util.ArrayList;

/**
 *
 * @author Ilya
 */
public class IteratorTable extends Table{
    private int count = -1;
    
    public IteratorTable(Table table) {
    this.asName = table.getAsName();
    this.head = table.getHead();
    this.rows = table.getRows();
    }
    
    public boolean nextrow(){
        count++;
        return rows.size() > count;
    }
    
    public Rowt getRow(){
        return rows.get(count);
    }
    
    public ArrayList<String> getHeadN(){
        return getHead();
    }
    
    public String getcollum(String name){                
        return rows.get(count).getColumns().get(head.indexOf(name));
    }
    
    public String getcollum(int id){        
        return rows.get(count).getColumns().get(id);
    }
    
    public String getcollumName(String name){ 
        try{
            return rows.get(count).getColumns().get(asName.indexOf(name));
        }catch(Exception ex){
            System.out.println("Error:"+ex);
            System.out.println("Get Name:"+name);
        }
        return null;
    }
    
}
