package com.xml.data.table;
import java.util.ArrayList;
/**
 * @author Ilya
 */
public class Table {
    public ArrayList<String> head; //Список заголовков полей таблицы.
    public ArrayList<String> asName; //Список имён полей. 
    public ArrayList<Rowt> rows; // Строки таблицы.
    
    
  

    public Table() { 
        this.head =  new ArrayList<>();
        this.rows =  new ArrayList<>();
        this.asName = new ArrayList<>();//Test
    }
    
    public void addRows(String Name) { //Добавление имени поля.
        asName.add(Name);
    }  
    
    public ArrayList<String> getAsName() {//Получение списка имён.
        return asName;
    }  
    
    public void addRows(Rowt row) { //Добавление Строки.
        rows.add(row);
    }  
    
    public ArrayList<Rowt> getRows() { //Получение строки.
        return rows;
    }  
    
  

    public void addHead(String name) { //Добавление заголовка поля.
        head.add(name);
    }
    
    public ArrayList<String> getHead() {// Получение списка заголовков.
        return head;
    }
            
}
