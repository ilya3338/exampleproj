/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xml.deserialization;

import com.xml.data.table.Rowt;
import com.xml.data.table.Table;
import com.xml.data.table.tag;
import com.xml.data.table.witchQualifier;
import com.xml.InterpreterSQL.InterpreterSQL;
import java.util.ArrayList;

/**
 *
 * @author Ilya
 */
public class DeserializationXML {
        private final StringBuffer xml = new StringBuffer(); //XML данные(не обработанные).
        private int iterCount = 0;//Счетчик необходим для работы мультистроки.
        private int iterCountQW = 0;//Счетчик необходим для работы мультистроки.
        
    public DeserializationXML(StringBuffer xml) {
        this.xml.append(xml);
    }

    public Table DeserializationDATAtoTableForPatch(String fromtag,boolean multirow,ArrayList<String> patchTags,ArrayList<String> asName){   
         Table table = new Table(); //Создание таблицы
         table.head.addAll(patchTags);// Заполнение Заголовков.
         table.asName.addAll(asName);// Заполнение Имён.
         do{
            StringBuffer data  = getTagValueRow(xml,fromtag);//Получение одной строки.
            if(data != null){
                Rowt row = new Rowt();//Одна строка из таблицы.
                for(String patchTag: patchTags){
                    String cell = getTagValueForPatch(data,patchTag);//Получение значения поля.
                    if(cell != null){
                        row.addColumns(cell);//Добавление  значение в поле строки.
                    }else
                    {
                     row.addColumns(null);//При отсутствии данных в поле или самого поля в ячейку выводить данное значение.
                    }
                }
                table.addRows(row);//Добавление заполненной строки в таблицу.
            }else
            {
                break;//Если строк больше не обнаруженно выходить из цикла.
            }
         }while(multirow);//Цикл выполняется пока не кончатся строки.         
         return table;
     }
     
    //Метод который возвращает строку с не обработанными данными.
    private StringBuffer getTagValueRow(StringBuffer in,String tagName){ 
        int id0;
         id0 = in.indexOf("<"+tagName,iterCount);
         if( id0 != -1 ) {
            int IdP = in.indexOf(">",id0); 
            int IdEnd = in.indexOf("</"+tagName+">",id0);
            if(IdEnd > 0)
                iterCount= IdEnd+(tagName.length()+3);
             return  new StringBuffer(in.substring(IdP+1,IdEnd)); 
         }
        return null;
    } 
     
    //Получение значения из тега по его пути или имени.
    private String getTagValueForPatch(StringBuffer xml,String patchTag){
         if(patchTag.contains("/")){
             String tag = patchTag.substring(0, patchTag.indexOf("/"));
             int index = xml.indexOf(tag);
             int proverka = patchTag.indexOf(".[with qualifier]");
             if(proverka >-1 && proverka<patchTag.indexOf("/")){
                 return getValueTagWithQualifier(xml,patchTag);
             }
             String test = patchTag.substring(patchTag.indexOf("/")+1);
            return getTagValueForPatch(new StringBuffer(getTagValueP(xml,tag)),test);
         }else
         {
             return getTagValueP(xml,patchTag);
         }         
    }
       
    private String getValueTagWithQualifier(StringBuffer xml,String tag){
         iterCountQW = 0;
         witchQualifier WQObj = new witchQualifier(tag);
         boolean multirow = true;
           do{
           StringBuffer data  = getTagValueRowQW(xml,WQObj.getPatchTagDataRow());//Получение одной строки.
           if(data != null){           
           boolean returndata = true;
           for(int indexParam = 0; indexParam < WQObj.getPatchTagParam().size(); indexParam++){
              String cell = getTagValueForPatch(data,WQObj.getPatchTagParam().get(indexParam));//Получение значения поля. 
              if(cell.equalsIgnoreCase(WQObj.getTagParamValue().get(indexParam)))
              {
                   returndata = true;
              }
              else 
              {
                   returndata = false;
                   break;
              }
           }           
           if(returndata){
               String pole = getTagValueForPatch(data,WQObj.getPatchTagData());//Получение значения поля. 
                return pole;
           }             
           }else
             break;
         }while(multirow);//Цикл выполняется пока не кончатся строки.
         return null;
     }
     
    //Метод который возваращает строку с не обработанными данными.
    private StringBuffer getTagValueRowQW(StringBuffer in,String tagName){ 
        int id0;
         id0 = in.indexOf("<"+tagName,iterCountQW);
         if( id0 != -1 ) {
            int IdP = in.indexOf(">",id0); 
            int IdEnd = in.indexOf("</"+tagName+">",id0);
            if(IdEnd > 0)
                iterCountQW= IdEnd+(tagName.length()+3);
            return  new StringBuffer(in.substring(IdP+1,IdEnd)); 
         }         
        return null;
    } 
     
    //Получение значения из тега по его имени.
    private static String getTagValueP(StringBuffer in,String tagName){                                 
         tag TagData = InterpreterSQL.poleTreatmentParam(tagName);          
         if(TagData!=null){
         int index = 0;
            while(true){
               int id0 = in.indexOf("<"+TagData.getNameTag()+" ",index);
               if( id0 != -1 ) {
                    int IdP = in.indexOf(">",id0); 
                    int paramS = in.indexOf(TagData.getParam()+"=\"",id0)+TagData.getParam().length()+2;
                    if(paramS > 0 && paramS < IdP) {
                            int paramE = in.indexOf("\"",paramS);             
                            if(paramE > 0  && paramE < IdP)        
                                return  in.substring(paramS,paramE);
                    }
                    index = IdP+1;
               }else break;
            }
         }else
         {
            TagData = InterpreterSQL.poleTreatmentParams(tagName); 
            if(TagData!=null){
               int index = 0;
               while(true){
                    int id0 = in.indexOf("<"+TagData.getNameTag()+" ",index);
                    if( id0 != -1 ) {
                         int IdP = in.indexOf(">",id0); 
                         String tagParam = in.substring(id0,IdP);
                         boolean paramValidate = false;
                         for(int countNum = 0; countNum < TagData.getParams().size();countNum++){
                              if(tagParam.contains(TagData.getParams().get(countNum)+"=\""+TagData.getParamsValue().get(countNum)+"\"")){
                                paramValidate = true;  
                              }else
                              {
                                  paramValidate=false;
                                  break;
                              }
                         }
                         if(paramValidate){
                             if(TagData.getParam() != null){
                                int paramS = in.indexOf(TagData.getParam()+"=\"",id0)+TagData.getParam().length()+2;
                                if(paramS > 0 && paramS < IdP) {
                                        int paramE = in.indexOf("\"",paramS);                          
                                        if(paramE > 0  && paramE < IdP)        
                                            return  in.substring(paramS,paramE);
                                }
                            }else
                             {
                                 int IdEnd = in.indexOf("</"+TagData.getNameTag()+">",id0);
                                 if(IdEnd > 0)        
                                    return  in.substring(IdP+1,IdEnd);
                             }
                         }
                         index = IdP+1;
                    }else break;
               }
            }else
            {
                int id0;
                id0 = in.indexOf("<"+tagName); 
                if( id0 == -1 )
                    id0 = in.indexOf("<"+tagName+">");                  
                        if( id0 != -1 ) {
                            int IdP = in.indexOf(">",id0); 
                            int IdEnd = in.indexOf("</"+tagName+">",id0);
                            if(IdEnd > 0)            
                            return  in.substring(IdP+1,IdEnd);  
                        }
            }    
            
         }         
        return null;
    } 
                               
}
