/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xml.InterpreterSQL;

import com.xml.data.table.Table;
import com.xml.data.table.tag;
import com.xml.deserialization.DeserializationXML;
import java.util.ArrayList;

/**
 *
 * @author Ilya
 */
public class InterpreterSQL {      
    
    public Table executeQueryFull(String Query,StringBuffer xml){
        DeserializationXML des = new DeserializationXML(xml); //Создание объекта Десериализатора FULL.
        boolean multi = !Query.contains("top 1");//Проверка запроса на параметр мультистроки(Если данный параметр присутствует то будет создана только одна строка).
        return des.DeserializationDATAtoTableForPatch( //Запуск Десериализатора
                getValue(Query,"from"), //Параметр Имя Таблицы.
                multi, //Параметр мультистроки.
                getListFields(Query),//Параметр со списком полей которые нужно обработать.
                getListNameFileds(Query)); //Список имён.
    }
    
    //Метод для получения имени таблицы из запроса.
    private String getValue(String Query,String FirstName){         
        int id0 = Query.indexOf(FirstName);
        if( id0 != -1 ) {
            return  Query.substring(id0+(FirstName.length()+1)).trim();                
        }
        return "";
    }
     
    //Метод получения списка полей из запроса.
    private ArrayList<String> getListFields(String in){     
     in = getFields(in);//Из полученной строки с полями будет создан список с полями.
     ArrayList<String> Fields = new ArrayList();
     int indexStart = 0;
     while(true){
         int index = in.indexOf(",",indexStart);
         if(index > 0){
         int indexAs = in.indexOf(" as ", indexStart);
         if(indexAs > 0 && indexAs < index){
        Fields.add(in.substring(indexStart,indexAs).trim());   
         }else
        Fields.add(in.substring(indexStart,index).trim());        
         }else
         {
         int indexAs = in.indexOf(" as ", indexStart);
         if(indexAs > 0){
            Fields.add(in.substring(indexStart,indexAs).trim());   
         }else{
            Fields.add(in.substring(indexStart,in.length()).trim());}         
          break;
         }
         indexStart = index+1;
     }
     
     return Fields;    
     }
     
    public static tag poleTreatmentParam(String in){         
         if(in.indexOf(".") > 0 && !in.contains(".(")){
            String NameTag;
            String Param;
            NameTag = in.substring(0, in.indexOf(".")).trim();
            Param = in.substring(in.indexOf(".")+1).trim();  
            return new tag(null, null, NameTag, Param);
         }                  
         return null;
     }
     
    public static tag poleTreatmentParams(String in){
         if(in.contains(".(")){
            ArrayList<String> Params = new ArrayList();
            ArrayList<String> ParamsValue = new ArrayList();
            String NameTag;
            String Param = null;
            NameTag = in.substring(0, in.indexOf(".")).trim();         
            if(in.indexOf(".") != in.lastIndexOf(".(")){
            Param = in.substring(in.indexOf(".")+1,in.lastIndexOf(".")).trim();  }
            String data = in.substring(in.indexOf(".(")+2,in.lastIndexOf(")"));
            int startIndex = 0;//data.indexOf(",");
            do{             
                if(data.indexOf(";",startIndex)>0){
                  String par = data.substring(startIndex,data.indexOf(";",startIndex));
                  Params.add(par.substring(0, par.indexOf("=")).trim());
                  ParamsValue.add(par.substring(par.indexOf("=")+1));
                  startIndex = data.indexOf(";",startIndex)+1;
                }else
                {
                  String par = data.substring(startIndex);
                  Params.add(par.substring(0, par.indexOf("=")).trim());
                  ParamsValue.add(par.substring(par.indexOf("=")+1).trim());               
                }
            }while(data.indexOf(";",startIndex)>0);  
             if(data.indexOf(";",0)>0 && data.indexOf(";",startIndex)== -1){
                  String par = data.substring(data.indexOf(";",0)+1);
                  Params.add(par.substring(0, par.indexOf("=")).trim());
                  ParamsValue.add(par.substring(par.indexOf("=")+1));
                  startIndex = data.indexOf(";",startIndex)+1;   
             }
            return new tag(Params, ParamsValue, NameTag, Param);
         }           
         return null;
     }
     
    //Метод получения списка имён полей из запроса.
    public ArrayList<String> getListNameFileds(String in){
         ArrayList<String> NameFields = new ArrayList();
         in = getFields(in);
         int index = 0;
         while(in.indexOf(",",index)>0){
         int end = in.indexOf(",",index);
         String pole = in.substring(index,end);
         index = end+1;
         if(pole.contains(" as " )){
             NameFields.add(pole.substring(pole.indexOf(" as ")+3).trim());
         }
         else{
          NameFields.add(pole);
         }
         }         
         String pole = in.substring(index);
         if(pole.contains(" as ")){
             NameFields.add(pole.substring(pole.indexOf(" as ")+3).trim());
         }
         else{
          NameFields.add(pole.trim());
         }
         return NameFields;
     }
     
    //Метод извлечения полей из запроса.
    private String getFields(String in){ 
        int id0;
         id0 = in.indexOf("select");
         if( id0 != -1 ) {
            int IdEnd = in.indexOf("from",id0);
            if(IdEnd > 0)
             return  in.substring(id0+6,IdEnd);             
        }
        return "";
    }     
    
}
